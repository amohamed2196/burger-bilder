import React from 'react'
import classes from './BuildControls.css'
import BuildControl from './BuildControl/BuildControl'

const controls= [
    {label: 'Salad' , type: 'salad'},
    {label: 'Meat' , type: 'meat'},
    {label: 'Bacon' , type: 'bacon'},
    {label: 'Cheese' , type: 'cheese'},
];
const BuildControls = (props) => (
    <div className={classes.BuildControls}>
        <p>Current Price is : <strong>{props.price.toFixed(2)}</strong></p>
        {controls.map(ctrl => (
            <BuildControl 
            key={ctrl.label} 
            label={ctrl.label}
            added = {()=>props.ingredientAdded(ctrl.type)}
            removed  = {()=>props.ingredientSub(ctrl.type)}
            disabledLess = {props.disableLess[ctrl.type]}
            disabledMore = {props.disableMore[ctrl.type]}
            />
        ))}
        <button 
        className={classes.OrderButton}
        disabled={!props.Purchasable}
        onClick={props.ordered}
        >
            ORDER NOW 
            </button>
    </div>
)

export default BuildControls;